#############################
# Azure Configuration       #
#############################

# Configure the azure provider
provider "azurerm" {
  version         = "~> 1.20"
  subscription_id = "${var.subscription_id}"
  client_id       = "${var.client_id}"
  client_secret   = "${var.client_secret}"
  tenant_id       = "${var.tenant_id}"
}

# Create a resource group
resource "azurerm_resource_group" "demo-rg" {
  name     = "${var.name_prefix}env"
  location = "${var.location}"
}

# Create an availability set
resource "azurerm_availability_set" "demo-avail" {
  name                = "${var.name_prefix}avail"
  location            = "${azurerm_resource_group.demo-rg.location}"
  resource_group_name = "${azurerm_resource_group.demo-rg.name}"
  managed             = true
}

# Create a virtual network
resource "azurerm_virtual_network" "demo-vnet" {
  name                = "${var.name_prefix}vnet"
  resource_group_name = "${azurerm_resource_group.demo-rg.name}"
  location            = "${azurerm_resource_group.demo-rg.location}"
  address_space       = ["10.0.0.0/16"]
}

# Create webservers subnet
resource "azurerm_subnet" "demo-sub-web" {
  name                 = "${var.name_prefix}sub-web"
  resource_group_name  = "${azurerm_resource_group.demo-rg.name}"
  virtual_network_name = "${azurerm_virtual_network.demo-vnet.name}"
  address_prefix       = "10.0.1.0/24"
}

# Create a public ip for use by load balancer
resource "azurerm_public_ip" "demo-ip-lb" {
  name                         = "${var.name_prefix}ip-lb"
  resource_group_name          = "${azurerm_resource_group.demo-rg.name}"
  location                     = "${azurerm_resource_group.demo-rg.location}"
  public_ip_address_allocation = "static"

  # domain_name_label            = "tbd"
}

# Create webservers load balancer
resource "azurerm_lb" "demo-lb-web" {
  name                = "${var.name_prefix}lb-web"
  resource_group_name = "${azurerm_resource_group.demo-rg.name}"
  location            = "${azurerm_resource_group.demo-rg.location}"

  frontend_ip_configuration {
    name                 = "${azurerm_public_ip.demo-ip-lb.name}"
    public_ip_address_id = "${azurerm_public_ip.demo-ip-lb.id}"
  }
}

# Add the backend for webserver LB
resource "azurerm_lb_backend_address_pool" "demo-lb-web-backend" {
  name                = "${var.name_prefix}lb-web-back"
  resource_group_name = "${azurerm_resource_group.demo-rg.name}"
  loadbalancer_id     = "${azurerm_lb.demo-lb-web.id}"
}

# Create HTTP probe on port 80
resource "azurerm_lb_probe" "demo-lb-tcp80probe" {
  name                = "${var.name_prefix}lb-tcp80probe"
  resource_group_name = "${azurerm_resource_group.demo-rg.name}"
  loadbalancer_id     = "${azurerm_lb.demo-lb-web.id}"
  protocol            = "tcp"
  port                = 80
}

# Create LB rule for HTTP and add to webserver LB
resource "azurerm_lb_rule" "demo-lb-tcp80rule" {
  name                           = "${var.name_prefix}lb-tcp80rule"
  resource_group_name            = "${azurerm_resource_group.demo-rg.name}"
  loadbalancer_id                = "${azurerm_lb.demo-lb-web.id}"
  protocol                       = "Tcp"
  frontend_port                  = "80"
  backend_port                   = "80"
  frontend_ip_configuration_name = "${azurerm_public_ip.demo-ip-lb.name}"
  probe_id                       = "${azurerm_lb_probe.demo-lb-tcp80probe.id}"
  backend_address_pool_id        = "${azurerm_lb_backend_address_pool.demo-lb-web-backend.id}"
}

# Create storage account
resource "azurerm_storage_account" "demo-sa" {
  name                     = "meierdemosa"
  resource_group_name      = "${azurerm_resource_group.demo-rg.name}"
  location                 = "${var.location}"
  account_tier             = "Standard"
  account_replication_type = "LRS"
}

# Create storage container
resource "azurerm_storage_container" "demo-sa-container" {
  name                  = "${var.name_prefix}sa-container"
  resource_group_name   = "${azurerm_resource_group.demo-rg.name}"
  storage_account_name  = "${azurerm_storage_account.demo-sa.name}"
  container_access_type = "private"
}

# Create Docker Swarm manager VM
resource "azurerm_network_interface" "demo-nic-swarm-manager" {
  name                = "${var.name_prefix}nic-swarm-manager"
  location            = "${var.location}"
  resource_group_name = "${azurerm_resource_group.demo-rg.name}"

  ip_configuration {
    name                          = "swarm-manager-ip"
    subnet_id                     = "${azurerm_subnet.demo-sub-web.id}"
    private_ip_address_allocation = "dynamic"
    public_ip_address_id          = "${azurerm_public_ip.demo-pubip-swarm-manager.id}"
  }
}

resource "azurerm_network_interface_backend_address_pool_association" "demo-lba-swarm-manager" {
  network_interface_id    = "${azurerm_network_interface.demo-nic-swarm-manager.id}"
  ip_configuration_name   = "swarm-manager-ip"
  backend_address_pool_id = "${azurerm_lb_backend_address_pool.demo-lb-web-backend.id}"
}

resource "azurerm_public_ip" "demo-pubip-swarm-manager" {
  name                         = "${var.name_prefix}pubip-swarm-manager"
  location                     = "${var.location}"
  resource_group_name          = "${azurerm_resource_group.demo-rg.name}"
  public_ip_address_allocation = "dynamic"
}

resource "azurerm_virtual_machine" "demo-vm-swarm-manager" {
  name                  = "${var.name_prefix}vm-swarm-manager"
  location              = "${var.location}"
  resource_group_name   = "${azurerm_resource_group.demo-rg.name}"
  network_interface_ids = ["${azurerm_network_interface.demo-nic-swarm-manager.id}"]
  availability_set_id   = "${azurerm_availability_set.demo-avail.id}"
  vm_size               = "Standard_DS1_v2"

  storage_os_disk {
    name              = "osDiskManager0"
    caching           = "ReadWrite"
    create_option     = "FromImage"
    managed_disk_type = "Premium_LRS"
  }

  storage_image_reference {
    publisher = "Canonical"
    offer     = "UbuntuServer"
    sku       = "16.04.0-LTS"
    version   = "latest"
  }

  os_profile {
    computer_name  = "${var.name_prefix}vm-swarm-manager"
    admin_username = "azureuser"
  }

  os_profile_linux_config {
    disable_password_authentication = true

    ssh_keys {
      path     = "/home/azureuser/.ssh/authorized_keys"
      key_data = "${var.public_key}"
    }
  }

  boot_diagnostics {
    enabled     = "true"
    storage_uri = "${azurerm_storage_account.demo-sa.primary_blob_endpoint}"
  }

  provisioner "remote-exec" {
    connection {
      type        = "ssh"
      user        = "azureuser"
      private_key = "${file(var.private_key_path)}"
    }

    inline = [
      "sudo apt update -y",

      # "sudo apt upgrade -y",
      "curl -fsSL https://get.docker.com -o get-docker.sh",

      "chmod +x get-docker.sh",
      "sudo ./get-docker.sh",
      "sudo usermod -aG docker azureuser",
      "sudo docker swarm init --advertise-addr ${azurerm_network_interface.demo-nic-swarm-manager.private_ip_address}",
      "sudo docker service create --name whoami --mode global --publish 80:8000 jwilder/whoami",
      "sudo docker service create --name=visualizer --publish=8080:8080/tcp --constraint=node.role==manager --mount=type=bind,src=/var/run/docker.sock,dst=/var/run/docker.sock dockersamples/visualizer",
    ]
  }
}

# Get Docker Swarm worker join token
data "azurerm_public_ip" "swarm_manager_data" {
  name                = "${azurerm_public_ip.demo-pubip-swarm-manager.name}"
  resource_group_name = "${azurerm_virtual_machine.demo-vm-swarm-manager.resource_group_name}"
}

data "external" "swarm_worker_token" {
  program = ["./scripts/get-join-tokens.sh"]

  query = {
    host = "${data.azurerm_public_ip.swarm_manager_data.ip_address}"
  }
}

# Create Docker Swarm worker VM
resource "azurerm_network_interface" "demo-nic-swarm-worker" {
  count               = "${var.worker_count}"
  name                = "demo-nic-swarm-worker${count.index}"
  location            = "${var.location}"
  resource_group_name = "${azurerm_resource_group.demo-rg.name}"

  ip_configuration {
    name                          = "swarm-worker-ip${count.index}"
    subnet_id                     = "${azurerm_subnet.demo-sub-web.id}"
    private_ip_address_allocation = "dynamic"
    public_ip_address_id          = "${length(azurerm_public_ip.demo-pubip-swarm-worker.*.id) > 0 ? element(concat(azurerm_public_ip.demo-pubip-swarm-worker.*.id, list("")), count.index) : ""}"

    #load_balancer_backend_address_pools_ids = ["${azurerm_lb_backend_address_pool.demo-lb-web-backend.id}"]
  }
}

resource "azurerm_network_interface_backend_address_pool_association" "demo-lba-swarm-worker" {
  count                   = "${var.worker_count}"
  network_interface_id    = "${element(azurerm_network_interface.demo-nic-swarm-worker.*.id, count.index)}"
  ip_configuration_name   = "swarm-worker-ip${count.index}"
  backend_address_pool_id = "${azurerm_lb_backend_address_pool.demo-lb-web-backend.id}"
}

resource "azurerm_public_ip" "demo-pubip-swarm-worker" {
  count                        = "${var.worker_count}"
  name                         = "${var.name_prefix}pubip-swarm-worker${count.index}"
  location                     = "${var.location}"
  resource_group_name          = "${azurerm_resource_group.demo-rg.name}"
  public_ip_address_allocation = "dynamic"
}

resource "azurerm_virtual_machine" "demo-vm-swarm-worker" {
  count                 = "${var.worker_count}"
  name                  = "${var.name_prefix}vm-swarm-worker${count.index}"
  location              = "${var.location}"
  resource_group_name   = "${azurerm_resource_group.demo-rg.name}"
  network_interface_ids = ["${element(azurerm_network_interface.demo-nic-swarm-worker.*.id, count.index)}"]
  availability_set_id   = "${azurerm_availability_set.demo-avail.id}"

  vm_size = "Standard_DS1_v2"

  storage_os_disk {
    name              = "osDiskWorker${count.index}"
    caching           = "ReadWrite"
    create_option     = "FromImage"
    managed_disk_type = "Premium_LRS"
  }

  storage_image_reference {
    publisher = "Canonical"
    offer     = "UbuntuServer"
    sku       = "16.04.0-LTS"
    version   = "latest"
  }

  os_profile {
    computer_name  = "${var.name_prefix}vm-swarm-worker${count.index}"
    admin_username = "azureuser"
  }

  os_profile_linux_config {
    disable_password_authentication = true

    ssh_keys {
      path     = "/home/azureuser/.ssh/authorized_keys"
      key_data = "${var.public_key}"
    }
  }

  boot_diagnostics {
    enabled     = "true"
    storage_uri = "${azurerm_storage_account.demo-sa.primary_blob_endpoint}"
  }

  provisioner "remote-exec" {
    connection {
      type        = "ssh"
      user        = "azureuser"
      private_key = "${file(var.private_key_path)}"
    }

    inline = [
      "sudo apt update -y",

      # "sudo apt upgrade -y",
      "curl -fsSL https://get.docker.com -o get-docker.sh",

      "chmod +x get-docker.sh",
      "sudo ./get-docker.sh",
      "sudo usermod -aG docker azureuser",
      "sudo docker swarm join --token ${data.external.swarm_worker_token.result.worker} ${azurerm_network_interface.demo-nic-swarm-manager.private_ip_address}:2377",
    ]
  }
}

# Output
output "manager_ip" {
  value = "${data.azurerm_public_ip.swarm_manager_data.ip_address}"
}

output "loadbalancer_ip" {
  value = "${azurerm_public_ip.demo-ip-lb.ip_address}"
}
