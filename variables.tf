#############################
# Azure Configuration       #
#############################
variable "location" {
  default = "East US"
}

variable "name_prefix" {
  default = "default-"
}

variable "private_key_path" {
  default = "~/.ssh/id_rsa"
}

variable "public_key" {}

variable "worker_count" {
  default = 1
}

variable "client_id" {}
variable "client_secret" {}
variable "subscription_id" {}
variable "tenant_id" {}
